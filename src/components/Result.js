
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

const cara = require('../imgs/moeda_cara.png');
const coroa = require('../imgs/moeda_coroa.png');

export default class Result extends React.Component {

    constructor(props) {
        super(props);

        this.state = { result: '' };
    }

    componentWillMount() {
        const num = Math.floor(Math.random() * 2);
        let coin = '';

        if (num === 0) {
            coin = 'cara';
        } else {
            coin = 'coroa';
        }

        this.setState({ result: coin });
    }

    render() {
        if (this.state.result === 'cara') {
            return (
                <View style={styles.container}>
                    <Image source={cara} />
                </View>
            );
        } 
        return (
            <View style={styles.container}>
                <Image source={coroa} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#61BD8C',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
