
import React from 'react';
import { StyleSheet, View, Image, TouchableHighlight } from 'react-native';

import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');
const btnPlay = require('../imgs/botao_jogar.png');
const btnInfo = require('../imgs/sobre_jogo.png');
const btnOtherGames = require('../imgs/outros_jogos.png');

export default class Main extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <Image source={logo} />
                    <TouchableHighlight
                        onPress={() => { Actions.Result(); }}
                    >
                        <Image source={btnPlay} />
                    </TouchableHighlight>
                </View>
                <View style={styles.bottom}>
                    <TouchableHighlight
                        onPress={() => { Actions.About(); }}
                    >
                        <Image source={btnInfo} />
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={() => { Actions.OtherGames(); }}
                    >
                        <Image source={btnOtherGames} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#61BD8C',
    },
    body: {
      flex: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    bottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }
  });
