
import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Main from './components/Main';
import About from './components/About';
import OtherGames from './components/OtherGames';
import Result from './components/Result';

const routes = () => (
    <Router sceneStyle={{ paddingTop: 50 }}>
        <Scene key='main' component={Main} initial title="Cara ou Coroa?" />
        <Scene key='About' component={About} title="Sobre o Jogo" />
        <Scene key='OtherGames' component={OtherGames} title="Outros Jogos" />
        <Scene key='Result' component={Result} title="Resultado" />
    </Router>
);
export default routes;
